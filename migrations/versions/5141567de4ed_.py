"""empty message

Revision ID: 5141567de4ed
Revises: 03dfeef28114
Create Date: 2023-05-11 14:12:41.215872

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5141567de4ed'
down_revision = '03dfeef28114'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('settings', schema=None) as batch_op:
        batch_op.add_column(sa.Column('can_subscribe', sa.Boolean(), nullable=True))
        batch_op.drop_column('subscribe')

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('settings', schema=None) as batch_op:
        batch_op.add_column(sa.Column('subscribe', sa.BOOLEAN(), nullable=True))
        batch_op.drop_column('can_subscribe')

    # ### end Alembic commands ###
