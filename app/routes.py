# encoding: utf-8

from app.controller.admin import Admin
from app.controller.core import Core
from app.controller.discussion import Discussion
from app.controller.proposal import Proposal
from app.controller.user import Community, User


# Listing endpoints
routes = [
    # Core
    ("/", Core.as_view("index"), ["GET", "POST"]),
    ("/search", Core.as_view("search")),
    ("/who", Core.as_view("who")),
    ("/why", Core.as_view("why")),
    ("/manifesto", Core.as_view("manifesto")),
    ("/cgu", Core.as_view("cgu")),
    ("/rgpd", Core.as_view("rgpd")),
    ("/legal", Core.as_view("legal")),
    ("/charter", Core.as_view("charter")),
    ("/faq", Core.as_view("faq")),
    ("/contact", Core.as_view("contact")),
    ("/status", Core.as_view("status")),
    # Administration
    ("/admin/dashboard", Admin.as_view("dashboard")),
    ("/admin/settings", Admin.as_view("settings"), ["GET", "POST"]),
    ("/admin/rules", Admin.as_view("rules")),
    ("/admin/rules/add", Admin.as_view("rule_new"), ["POST"]),
    # User
    ("/register", User.as_view("register"), ["GET", "POST"]),
    ("/login", User.as_view("login"), ["GET", "POST"]),
    ("/forgot_password", User.as_view("forgot_password"), ["GET", "POST"]),
    ("/logout", User.as_view("logout")),
    ("/profile", User.as_view("profile"), ["GET", "POST"]),
    ("/communities", User.as_view("communities")),
    ("/community", Community.as_view("list")),
    ("/community/new", Community.as_view("new"), ["GET", "POST"]),
    ("/community/edit/<id>", Community.as_view("edit"), ["GET", "POST"]),
    ("/community/join/<id>", Community.as_view("join")),
    ("/community/quit/<id>", Community.as_view("quit"), ["GET", "POST"]),
    # Proposal
    ("/proposal/", Proposal.as_view("list")),
    ("/proposal/new", Proposal.as_view("new"), ["GET", "POST"]),
    ("/proposal/<id>", Proposal.as_view("view")),
    ("/proposal/edit/<id>", Proposal.as_view("edit"), ["GET", "POST"]),
    ("/proposal/delete/<id>", Proposal.as_view("delete")),
    # Discussion
    ("/discussion/", Discussion.as_view("list")),
    ("/discussion/new/<id>", Discussion.as_view("new"), ["GET", "POST"]),
    ("/discussion/<id>", Discussion.as_view("view")),
    ("/discussion/send", Discussion.as_view("send"), ["POST"]),
    ("/discussion/delete/<id>", Discussion.as_view("delete")),
]

# Listing API endpoints
apis = []
