# encoding: utf-8

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required, login_user, logout_user

from app.controller.controller import Controller
from app.form.admin import AddRuleForm, SettingsForm
from app.model.admin import RuleModel, SettingsModel
from app.model.user import CommunityModel, UserModel


class Admin(Controller):
    @login_required
    def dashboard(self):
        if not current_user.is_admin:
            return redirect(url_for("core.index"))
        g.users = UserModel.query.count()
        g.communities = CommunityModel.query.all()
        return render_template("admin/dashboard.html")

    @login_required
    def settings(self):
        if not current_user.is_admin:
            return redirect(url_for("core.index"))
        g.form = SettingsForm(obj=g.settings)
        if request.method == "POST":
            if g.form.validate_on_submit():
                g.settings.can_subscribe = g.form.can_subscribe.data
                g.settings.can_create_community = g.form.can_create_community.data
                g.settings.save()
                return redirect(url_for("admin.settings"))

        return render_template("admin/settings.html")

    @login_required
    def rules(self):
        if not current_user.is_admin:
            return redirect(url_for("core.index"))
        g.rules = RuleModel.query.all()
        g.form = AddRuleForm()

        return render_template("admin/rules.html")

    @login_required
    def rule_new(self):
        if not current_user.is_admin:
            return redirect(url_for("core.index"))
        g.form = AddRuleForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                rule = RuleModel(rule=g.form.rule.data)
                rule.save()

        return redirect(url_for("admin.rules"))
