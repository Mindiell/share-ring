# encoding: utf-8

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required, login_user, logout_user

from app.controller.controller import Controller
from app.form.user import *
from app.model.user import CommunityModel, MemberModel, UserModel


class User(Controller):
    def login(self):
        g.form = UserLoginForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(login=g.form.login.data).first()
                if user is not None and user.check_password(g.form.password.data):
                    if user.is_active:
                        login_user(user)
                        if len(user.communities) > 0:
                            return redirect(url_for("core.search"))
                        return redirect(url_for("user.profile"))
                    else:
                        flash(
                            _(
                                "Votre compte a été désactivé. En cas de problème, "
                                "vous pouvez contacter l'équipe de support."
                            ),
                            "warning",
                        )
                else:
                    flash(
                        _("Identifiant inconnu ou mauvais mot de passe."),
                        "warning",
                    )
        return render_template("user/login.html")

    @login_required
    def logout(self):
        session.clear()
        return redirect(url_for("core.index"))

    def register(self):
        g.form = UserRegisterForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(login=g.form.login.data).first()
                if user is None:
                    if g.form.password.data == g.form.password_confirmation.data:
                        user = UserModel(
                            login=g.form.login.data,
                            password=g.form.password.data,
                            email=g.form.email.data,
                            active=True,
                        )
                        user.save()
                        login_user(user)
                        return redirect(url_for("user.communities"))
                    else:
                        flash(
                            _("Les deux mots de passe ne sont pas identiques."),
                            "warning",
                        )
                else:
                    flash(
                        _(
                            "Cet identifiant n'est pas utilisable, "
                            "merci d'en sélectionner un autre."
                        ),
                        "warning",
                    )
        return render_template("user/register.html")

    def forgot_password(self):
        g.form = UserForgotPasswordForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(login=g.form.login.data).first()
                if user is None:
                    user = UserModel.query.filter_by(email=g.form.login.data).first()
                if user is not None:
                    # TODO: send email & create token
                    flash(_("Un e-mail a été envoyé."), "success")
                return redirect(url_for("core.index"))

        return render_template("user/forgot_password.html")

    @login_required
    def profile(self):
        g.form = UserProfileForm(obj=current_user)
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(id=current_user.id).first()
                user.email = g.form.email.data
                modified = False
                if (
                    user.latitude != g.form.latitude.data
                    or user.longitude != g.form.longitude.data
                ):
                    modified = True
                user.latitude = g.form.latitude.data
                user.longitude = g.form.longitude.data
                user.save()
                if modified:
                    # Update all proposals position
                    for proposal in user.proposals:
                        proposal.latitude = user.latitude
                        proposal.longitude = user.longitude
                        proposal.save()
                return redirect(url_for("user.profile"))
        return render_template("user/profile.html")

    @login_required
    def communities(self):
        return render_template("user/communities.html")


class Community(Controller):
    @login_required
    def list(self):
        name = request.args.get("name", None)
        g.form = CommunitySearchForm()
        g.form.name.data = name
        communities = CommunityModel.query
        if g.form.name.data is not None:
            communities = communities.filter(CommunityModel.name.like(f"%{name}%"))
        g.communities = communities.all()

        return render_template("user/community_list.html")

    @login_required
    def new(self):
        g.form = CommunityNewForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                community = CommunityModel.query.filter_by(
                    name=g.form.name.data
                ).first()
                if community is not None:
                    flash(
                        _("Ce nom semble déjà exister, merci d'en trouver un autre."),
                        "warning",
                    )
                else:
                    community = CommunityModel(name=g.form.name.data)
                    if g.form.latitude.data != "":
                        community.latitude = g.form.latitude.data
                    if g.form.longitude.data != "":
                        community.longitude = g.form.longitude.data
                    community.save()
                    # Attach this new community to current user
                    current_user.communities.append(community)
                    current_user.save()
                    # Make her manager of the community
                    community.membership[0].manager = True
                    community.membership[0].save()
                    flash(
                        _(f"La communauté {community.name} vient d'être créée !"),
                        "success",
                    )
                    return redirect(url_for("user.communities"))
        return render_template("user/community_new.html")

    @login_required
    def edit(self, id):
        if int(id) not in [
            membership.community_id for membership in current_user.membership
        ]:
            return redirect(url_for("user.communities"))
        community = CommunityModel.query.get(id)
        g.community_id = id
        g.form = CommunityNewForm(obj=community)
        if request.method == "POST":
            if g.form.validate_on_submit():
                if g.form.name.data != "":
                    community.name = g.form.name.data
                if g.form.latitude.data != "":
                    community.latitude = g.form.latitude.data
                if g.form.longitude.data != "":
                    community.longitude = g.form.longitude.data
                community.save()
                flash(_("La communauté a été modifiée"), "success")
                return redirect(url_for("user.communities"))

        return render_template("user/community_edit.html")

    @login_required
    def join(self, id):
        community = CommunityModel.query.get(id)
        membership = (
            MemberModel.query.filter_by(community_id=community.id)
            .filter_by(user_id=current_user.id)
            .first()
        )
        if membership is None:
            membership = MemberModel(
                community_id=community.id,
                user_id=current_user.id,
            )
            membership.save()

        return redirect(url_for("user.communities"))

    @login_required
    def quit(self, id):
        community = CommunityModel.query.get(id)
        membership = (
            MemberModel.query.filter_by(community_id=community.id)
            .filter_by(user_id=current_user.id)
            .first()
        )
        if membership is not None:
            membership.delete()
        # Manage last user of community
        if len(community.members) == 0:
            community.delete()

        return redirect(url_for("user.communities"))
