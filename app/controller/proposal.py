# encoding: utf-8

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required

from app.controller.controller import Controller
from app.form.proposal import ProposalForm, ProposalSearchForm
from app.model.proposal import ProposalModel


class Proposal(Controller):
    @login_required
    def list(self):
        return render_template("proposal/list.html")

    @login_required
    def new(self):
        g.form = ProposalForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                proposal = ProposalModel(
                    title=g.form.title.data,
                    description=g.form.description.data,
                    owner_id=current_user.id,
                    caution=g.form.caution.data,
                    duration=g.form.duration.data,
                )
                proposal.save()
                flash(_("Votre annonce est publiée !"), "success")
                return redirect(url_for("proposal.list"))

        return render_template("proposal/new.html")

    @login_required
    def view(self, id):
        g.proposal = ProposalModel.query.get(id)

        return render_template("proposal/view.html")

    @login_required
    def edit(self, id):
        proposal = ProposalModel.query.get(id)
        if current_user.id != proposal.owner_id:
            return redirect(url_for("core.index"))
        g.proposal_id = id
        g.form = ProposalForm(obj=proposal)
        if request.method == "POST":
            if g.form.validate_on_submit():
                proposal.title = g.form.title.data
                proposal.description = g.form.description.data
                proposal.caution = g.form.caution.data
                proposal.duration = g.form.duration.data
                proposal.save()
                flash(_("Votre objet est modifié"), "success")
                return redirect(url_for("proposal.list"))

        return render_template("proposal/edit.html")

    @login_required
    def delete(self, id):
        proposal = ProposalModel.query.get(id)
        if current_user.id != proposal.owner_id:
            return redirect(url_for("core.index"))
        if proposal is not None:
            proposal.delete()

        return redirect(url_for("proposal.list"))
