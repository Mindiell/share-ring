# encoding: utf-8

from flask import g

from app.model.admin import SettingsModel


# before_request action
def before():
    settings = SettingsModel.query.first()
    if settings is None:
        settings = SettingsModel()
        settings.save()
    g.settings = settings


# after_request action
def after(response):
    return response
