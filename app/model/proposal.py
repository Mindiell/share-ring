# encoding: utf-8

import math

from flask_login import current_user

from app.model.model import db, Model


class ProposalModel(db.Model, Model):
    __tablename__ = "proposal"
    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    owner = db.relationship("UserModel", backref="proposals")
    active = db.Column(db.Boolean, default=True)
    title = db.Column(db.String(100))
    description = db.Column(db.Text)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    caution = db.Column(db.Boolean, default=False)
    duration = db.Column(db.Integer, default=0)
    letter = db.Column(db.Boolean, default=False)

    def distance(self, latitude, longitude):
        """
        Compute distance with latitude and longitude and then simplify it.
        """
        if latitude is None or longitude is None:
            return 0
        x = (longitude - self.longitude) * math.cos((latitude + self.latitude) / 2)
        y = latitude - self.latitude
        z = math.sqrt(x * x + y * y)
        d = round(1852 * 60 * z)
        return int((round(d / 100) * 100 + round((d + 50) / 100) * 100) / 2)

    def on_after_create(self, model):
        if self.longitude is None:
            self.longitude = self.owner.longitude
        if self.latitude is None:
            self.latitude = self.owner.latitude
        self.save()

    def __repr__(self):
        return self.title
