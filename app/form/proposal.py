# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import BooleanField, IntegerField, StringField, TextAreaField
from wtforms.validators import DataRequired


class ProposalForm(FlaskForm):
    title = StringField(_("Titre de l'annonce"), validators=[DataRequired()])
    description = TextAreaField(
        _("Description"), render_kw={"rows": 6}, validators=[DataRequired()]
    )
    caution = BooleanField(
        _("Caution"),
        default=False,
        description=_(
            "Si vous le souhaitez, vous pouvez signaler que vous pouvez demander une "
            "caution en échange du prêt d'un objet."
        ),
    )
    duration = StringField(
        _("Durée maximale du prêt"),
        default=0,
        description=_("Durée maximale souhaitée pour un prêt, en jours."),
    )


class ProposalSearchForm(FlaskForm):
    q = StringField(_("Recherche"))
