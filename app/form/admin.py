# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import BooleanField, FieldList, FormField, HiddenField, TextAreaField
from wtforms.validators import DataRequired


class AddRuleForm(FlaskForm):
    rule = TextAreaField("Nouvelle règle")


class SettingsForm(FlaskForm):
    can_subscribe = BooleanField(_("Inscriptions autorisées"))
    can_create_community = BooleanField(_("Création de communauté autorisée"))
