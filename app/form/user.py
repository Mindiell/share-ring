# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import BooleanField, HiddenField, PasswordField, StringField
from wtforms.validators import DataRequired


class UserLoginForm(FlaskForm):
    login = StringField(_("Nom d'utilisateur"), validators=[DataRequired()])
    password = PasswordField(_("Mot de passe"), validators=[DataRequired()])


class UserRegisterForm(FlaskForm):
    login = StringField(
        _("Nom d'utilisateur"),
        validators=[DataRequired()],
        description=_(
            "Le nom d'utilisateur doit être unique. C'est ce nom que les autres "
            "utilisateurs verront."
        ),
    )
    password = PasswordField(
        _("Mot de passe"),
        validators=[DataRequired()],
        description=_(
            "Un bon mot de passe est un mot de passe long et facile à retenir. "
            "Choisissez un mot de passe différent pour chaque site et d'au moins "
            "12 caractères."
        ),
    )
    password_confirmation = PasswordField(
        _("Confirmation"),
        validators=[DataRequired()],
        description=_("Reprise du mot de passe pour confirmation."),
    )
    email = StringField(
        _("Adresse email"),
        description=_(
            "Ce champ est optionnel. L'adresse email est utilisée si vous perdez "
            "votre mot de passe et pour vous envoyer des alertes lorque vous recevez "
            "des messages (si l'option est activée)."
        ),
    )


class UserForgotPasswordForm(FlaskForm):
    login = StringField(
        _("Nom d'utilisateur ou adresse email"),
        validators=[DataRequired()],
        description=_(
            "Le nom d'utilisateur ou l'adresse email sera utilisée pour vous envoyer "
            "un e-mail vous permettant de modifier votre mot de passe."
        ),
    )


class UserResetPasswordForm(FlaskForm):
    token = HiddenField()
    password = PasswordField(
        _("Mot de passe"),
        validators=[DataRequired()],
        description=_(
            "Votre nouveau mot de passe. Un bon mot de passe est un mot de passe "
            "long et facile à retenir. Choisissez un mot de passe différent pour "
            "chaque site et d'au moins 12 caractères."
        ),
    )
    password_confirmation = PasswordField(
        _("Confirmation"),
        validators=[DataRequired()],
        description=_("Reprise du mot de passe pour confirmation."),
    )


class UserProfileForm(FlaskForm):
    email = StringField(
        _("Adresse mail"),
        description=_(
            "Ce champ est optionnel. L'adresse email est utilisée si vous perdez "
            "votre mot de passe et pour vous envoyer des alertes lorque vous recevez "
            "des messages (si l'option est activée)."
        ),
    )


class CommunityNewForm(FlaskForm):
    name = StringField(_("Nom de la communauté"), validators=[DataRequired()])
    longitude = HiddenField(_("Longitude"))
    latitude = HiddenField(_("Latitude"))


class CommunitySearchForm(FlaskForm):
    name = StringField(_("Nom de la communauté"))
    longitude = StringField(_("Longitude"))
    latitude = StringField(_("Latitude"))
